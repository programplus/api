-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-11-2021 a las 15:52:16
-- Versión del servidor: 10.3.31-MariaDB-0+deb10u1
-- Versión de PHP: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `admin_retatushabilidades_v2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro`
--

CREATE TABLE `centro` (
  `centro_id` int(11) NOT NULL,
  `centro_nombre` varchar(150) NOT NULL,
  `centro_estado` tinyint(4) NOT NULL,
  `regional_regional_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `centro`
--

INSERT INTO `centro` (`centro_id`, `centro_nombre`, `centro_estado`, `regional_regional_id`) VALUES
(1, 'Despacho Dirección', 1, 1),
(2, 'Centro para la Biodiversidad y el Turismo del Amazonas', 1, 1),
(3, 'Despacho Dirección', 1, 2),
(4, 'Centro de los Recursos Naturales Renovables -La Salada', 1, 2),
(5, 'Centro del Diseño y Manufactura del Cuero', 1, 2),
(6, 'Centro de Formación en Diseño, Confección y Moda.', 1, 2),
(7, 'Centro para el Desarrollo del Hábitat y la Construcción', 1, 2),
(8, 'Centro de Tecnología de la Manufactura Avanzada.', 1, 2),
(9, 'Centro Tecnológico del Mobiliario', 1, 2),
(10, 'Centro Textil y de Gestión Industrial', 1, 2),
(11, 'Centro de Comercio', 1, 2),
(12, 'Centro de Servicios de Salud', 1, 2),
(13, 'Centro de Servicios y Gestión Empresarial', 1, 2),
(14, 'Complejo Tecnológico para la Gestión Agroempresarial', 1, 2),
(15, 'Complejo Tecnológico Minero Agroempresarial', 1, 2),
(16, 'Centro de la Innovación, la Agroindustria y la Aviación', 1, 2),
(17, 'Complejo Tecnológico Agroindustrial, Pecuario y Turístico', 1, 2),
(18, 'Complejo Tecnológico, Turístico y Agroindustrial del Occidente Antioqueño', 1, 2),
(19, 'Centro de Formación Minero Ambiental', 1, 2),
(20, 'Despacho Dirección', 1, 3),
(21, 'Centro de Gestión y Desarrollo Agroindustrial de Arauca', 1, 3),
(22, 'Despacho Dirección', 1, 4),
(23, 'Centro para el Desarrollo Agroecologico y Agroindustrial', 1, 4),
(24, 'Centro Nacional Colombo Alemán', 1, 4),
(25, 'Centro Industrial y de Aviación', 1, 4),
(26, 'Centro de Comercio y Servicios', 1, 4),
(27, 'Despacho Dirección', 1, 5),
(28, 'Centro Agroempresarial y Minero', 1, 5),
(29, 'Centro Internacional Náutico, Fluvial y Portuario', 1, 5),
(30, 'Centro para la Industria Petroquímica', 1, 5),
(31, 'Centro de Comercio y Servicios', 1, 5),
(32, 'Despacho Dirección', 1, 6),
(33, 'Centro de Desarrollo Agropecuario y Agroindustrial', 1, 6),
(34, 'Centro Minero', 1, 6),
(35, 'Centro de Gestión Administrativa y Fortalecimiento Empresarial', 1, 6),
(36, 'Centro Industrial de Mantenimiento y Manufactura', 1, 6),
(37, 'Despacho Dirección', 1, 7),
(38, 'Centro para la Formación Cafetera', 1, 7),
(39, 'Centro de Automatización Industrial', 1, 7),
(40, 'Centro de Procesos Industriales y Construcción', 1, 7),
(41, 'Centro de Comercio y Servicios', 1, 7),
(42, 'Centro Pecuario y Agroempresarial', 1, 7),
(43, 'Despacho Dirección', 1, 8),
(44, 'Centro Tecnológico de la Amazonia', 1, 8),
(45, 'Despacho Dirección', 1, 9),
(46, 'Centro Agroindustrial y Fortalecimiento Empresarial de Casanare', 1, 9),
(47, 'Despacho Dirección', 1, 10),
(48, 'Centro Agropecuario', 1, 10),
(49, 'Centro de Teleinformática y Producción Industrial', 1, 10),
(50, 'Centro de Comercio y Servicios', 1, 10),
(51, 'Despacho Dirección', 1, 11),
(52, 'Centro Biotecnológico del Caribe', 1, 11),
(53, 'Centro Agroempresarial', 1, 11),
(54, 'Centro de Operación y Mantenimiento Minero', 1, 11),
(55, 'Despacho Dirección', 1, 12),
(56, 'Centro de Recursos Naturales, Industria y Biodiversidad', 1, 12),
(57, 'Despacho Dirección', 1, 13),
(58, 'Centro Industrial y de Desarrollo Empresarial de Soacha', 1, 13),
(59, 'Centro de Desarrollo Agroindustrial y Empresarial', 1, 13),
(60, 'Centro Agroecológico y Empresarial', 1, 13),
(61, 'Centro de la Tecnología de Diseño y la Productividad Empresarial', 1, 13),
(62, 'Centro de Biotecnología Agropecuaria', 1, 13),
(63, 'Centro de Desarrollo Agroempresarial', 1, 13),
(64, 'Despacho Dirección', 1, 14),
(65, 'Centro Agropecuario y de Biotecnología el Porvenir', 1, 14),
(66, 'Centro de Comercio, Industria y Turismo de Córdoba', 1, 14),
(67, 'Despacho Dirección', 1, 15),
(68, 'Oficina de Control Interno', 1, 15),
(69, 'Oficina de Control Interno Disciplinario', 1, 15),
(70, 'Oficina de Comunicaciones', 1, 15),
(71, 'Oficina de Sistemas', 1, 15),
(72, 'Dirección Jurídica', 1, 15),
(73, 'Secretaría General', 1, 15),
(74, 'Dirección de Planeación y Direccionamiento Corporativo', 1, 15),
(75, 'Dirección Administrativa y Financiera', 1, 15),
(76, 'Dirección de Empleo y Trabajo', 1, 15),
(77, 'Dirección de Formación Profesional', 1, 15),
(78, 'Dirección Sistema Nacional de Formación para el Trabajo', 1, 15),
(79, 'Dirección de Promoción y Relaciones Corporativas', 1, 15),
(80, 'Despacho Dirección', 1, 16),
(81, 'Centro de Tecnologías para la Construcción y la Madera', 1, 16),
(82, 'Centro de Electricidad, Electrónica y Telecomunicaciones', 1, 16),
(83, 'Centro de Gestión Industrial', 1, 16),
(84, 'Centro de Manufactura en Textil y Cuero', 1, 16),
(85, 'Centro de Tecnologías del Transporte', 1, 16),
(86, 'Centro Metalmecánico', 1, 16),
(87, 'Centro de Materiales y Ensayos', 1, 16),
(88, 'Centro de Diseño y Metrología', 1, 16),
(89, 'Centro para la Industria de la Comunicación Grafica', 1, 16),
(90, 'Centro de Gestión de Mercados, Logística y Tecnologías de la Información', 1, 16),
(91, 'Centro de Formación de Talento Humano en Salud', 1, 16),
(92, 'Centro de Gestión Administrativa', 1, 16),
(93, 'Centro de Servicios Financieros', 1, 16),
(94, 'Centro Nacional de Hoteleria, Turismo y Alimentos', 1, 16),
(95, 'Centro de Formación en Actividad Física y cultura', 1, 16),
(96, 'Despacho Dirección', 1, 17),
(97, 'Centro Ambiental y Ecoturístico del Nororiente Amazónico', 1, 17),
(98, 'Despacho Dirección', 1, 18),
(99, 'Centro Industrial y de Energías Alternativas', 1, 18),
(100, 'Centro Agroempresarial y Acuícola', 1, 18),
(101, 'Despacho Dirección', 1, 19),
(102, 'Centro de Desarrollo Agroindustrial, Turístico y Tecnológico del Guaviare', 1, 19),
(103, 'Despacho Dirección', 1, 20),
(104, 'Centro de Formación Agroindustrial', 1, 20),
(105, 'Centro Agroempresarial y Desarrollo Pecuario del Huila', 1, 20),
(106, 'Centro de Desarrollo Agroempresarial y Turístico del Huila', 1, 20),
(107, 'Centro de la Industria, la Empresa y los Servicios', 1, 20),
(108, 'Centro de Gestión y Desarrollo Sostenible Surcolombiano', 1, 20),
(109, 'Despacho Dirección', 1, 21),
(110, 'Centro Acuícola y Agroindustrial de Gaira', 1, 21),
(111, 'Centro de Logística y Promoción Ecoturística del Magdalena', 1, 21),
(112, 'Despacho Dirección', 1, 22),
(113, 'Centro Agroindustrial del Meta', 1, 22),
(114, 'Centro de Industria y Servicios del Meta', 1, 22),
(115, 'Despacho Dirección', 1, 23),
(116, 'Centro Sur Colombiano de Logística Internacional', 1, 23),
(117, 'Centro Agroindustrial y Pesquero de la Costa Pacífica', 1, 23),
(118, 'Centro Internacional de Producción Limpia - Lope', 1, 23),
(119, 'Despacho Dirección', 1, 24),
(120, 'Centro de Formación para el Desarrollo Rural y Minero', 1, 24),
(121, 'Centro de la Industria, la Empresa y los Servicios', 1, 24),
(122, 'Despacho Dirección', 1, 25),
(123, 'Centro Agroforestal y Acuícola Arapaima', 1, 25),
(124, 'Despacho Dirección', 1, 26),
(125, 'Centro Agroindustrial', 1, 26),
(126, 'Centro para el Desarrollo Tecnológico de la Construcción y la Industria', 1, 26),
(127, 'Centro de Comercio y Turismo', 1, 26),
(128, 'Despacho Dirección', 1, 27),
(129, 'Centro Atención Sector Agropecuario', 1, 27),
(130, 'Centro de Diseño e Innovación Tecnológica Industrial', 1, 27),
(131, 'Centro de Comercio y Servicios', 1, 27),
(132, 'Despacho Dirección', 1, 28),
(133, 'Centro de Formación Turística, Gente de Mar y de Servicios', 1, 28),
(134, 'Despacho Dirección', 1, 29),
(135, 'Centro Atención Sector Agropecuario', 1, 29),
(136, 'Centro Industrial de Mantenimiento Integral', 1, 29),
(137, 'Centro Industrial del Diseño y la Manufactura', 1, 29),
(138, 'Centro de Servicios Empresariales y Turísticos', 1, 29),
(139, 'Centro Industrial y del Desarrollo Tecnológico', 1, 29),
(140, 'Centro Agroturístico', 1, 29),
(141, 'Centro Agroempresarial y Turístico de los Andes', 1, 29),
(142, 'Centro de Gestión Agroempresarial del Oriente', 1, 29),
(143, 'Despacho Dirección', 1, 30),
(144, 'Centro de la Innovación, la Tecnología y los Servicios', 1, 30),
(145, 'Despacho Dirección', 1, 31),
(146, 'Centro Agropecuario la Granja', 1, 31),
(147, 'Centro de Industria y Construcción', 1, 31),
(148, 'Centro de Comercio y Servicios', 1, 31),
(149, 'Despacho Dirección', 1, 32),
(150, 'Centro Agropecuario de Buga', 1, 32),
(151, 'Centro Latinoamericano de Especies Menores', 1, 32),
(152, 'Centro Náutico Pesquero de Buenaventura', 1, 32),
(153, 'Centro de Electricidad y Automatización Industrial -CEAI', 1, 32),
(154, 'Centro de la Construcción', 1, 32),
(155, 'Centro de Diseño Tecnológico Industrial', 1, 32),
(156, 'Centro Nacional de Asistencia Técnica a la Industria -ASTIN', 1, 32),
(157, 'Centro de Gestión Tecnológica de Servicios', 1, 32),
(158, 'Centro de Tecnologías Agroindustriales', 1, 32),
(159, 'Centro de Biotecnología Industrial', 1, 32),
(160, 'Despacho Dirección', 1, 33),
(161, 'Centro Agropecuario y de Servicios Ambientales Jiri-jirimo', 1, 33),
(162, 'Despacho Dirección', 1, 34),
(163, 'Centro de Producción y Transformación Agroindustrial de la Orinoquia', 1, 34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidades`
--

CREATE TABLE `habilidades` (
  `habilidades_id` int(11) NOT NULL,
  `habilidades_item` varchar(100) NOT NULL,
  `habilidades_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `habilidades`
--

INSERT INTO `habilidades` (`habilidades_id`, `habilidades_item`, `habilidades_estado`) VALUES
(1, 'Trabajo en equipo', 1),
(2, 'Comunicación', 1),
(3, 'Creatividad', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegos`
--

CREATE TABLE `juegos` (
  `juegos_id` int(11) NOT NULL,
  `juegos_nombre` varchar(150) NOT NULL,
  `juegos_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `juegos`
--

INSERT INTO `juegos` (`juegos_id`, `juegos_nombre`, `juegos_estado`) VALUES
(1, 'Puzzle Alien', 1),
(2, 'Puente Flotante', 1),
(3, 'Pista Arácnida', 1),
(4, 'Castillo de Fantasmas', 1),
(5, 'Reto Creativo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegos_nivel`
--

CREATE TABLE `juegos_nivel` (
  `juegos_juegos_id` int(11) NOT NULL,
  `nivel_nivel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `juegos_nivel`
--

INSERT INTO `juegos_nivel` (`juegos_juegos_id`, `nivel_nivel_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

CREATE TABLE `nivel` (
  `nivel_id` int(11) NOT NULL,
  `nivel_numero` tinyint(4) NOT NULL COMMENT '1 - 2 - 3',
  `nivel_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nivel`
--

INSERT INTO `nivel` (`nivel_id`, `nivel_numero`, `nivel_estado`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `preguntas_id` int(11) NOT NULL,
  `preguntas_orden` int(11) NOT NULL,
  `preguntas_item` text NOT NULL,
  `preguntas_parent` int(11) NOT NULL,
  `tipo_pregunta_tipo_pregunta_id` int(11) NOT NULL,
  `habilidades_habilidades_id` int(11) NOT NULL,
  `test_test_id` int(11) NOT NULL,
  `preguntas_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`preguntas_id`, `preguntas_orden`, `preguntas_item`, `preguntas_parent`, `tipo_pregunta_tipo_pregunta_id`, `habilidades_habilidades_id`, `test_test_id`, `preguntas_estado`) VALUES
(241, 1, 'El éxito es mi meta y estilo de vida', 0, 1, 1, 1, 1),
(242, 2, 'Me gusta quedar por encima de los demás en las discusiones de grupo', 0, 1, 2, 1, 1),
(243, 3, 'Siento temor cuando miro a un papel en blanco / hoja de cálculo / pantalla / lienzo porque no estoy seguro de cómo empezar o de dónde van a venir las ideas', 0, 1, 3, 1, 1),
(244, 4, 'Antes de hacer algo tengo en cuenta el criterio de mis amigos', 0, 1, 1, 1, 1),
(245, 5, 'Cuando trabajo en grupo no soporto fácilmente los errores o deficiencias de los demás', 0, 1, 2, 1, 1),
(246, 6, 'Antes de Tomar una decisión importante, analizo las distintas alternativas posibles', 0, 1, 3, 1, 1),
(247, 7, 'La educación cívica y las buenas maneras son para otros', 0, 1, 1, 1, 1),
(248, 8, 'Me gusta planificar las cosas con antelación', 0, 1, 2, 1, 1),
(249, 9, 'Paso mucho tiempo pensando en lo que voy a hacer, en lugar de hacerlo.', 0, 1, 3, 1, 1),
(250, 10, 'Tengo una gran capacidad de trabajo', 0, 1, 1, 1, 1),
(251, 11, 'Me gusta organizar y participar en actividades sociales', 0, 1, 2, 1, 1),
(252, 12, 'Traiga cualquier palabra o tema al azar, y todavía tendré algo que decir al respecto', 0, 1, 3, 1, 1),
(253, 13, 'Fácilmente abandono las obligaciones', 0, 1, 1, 1, 1),
(254, 14, 'Me encanta comunicar mis tristezas y alegrías a los demás', 0, 1, 2, 1, 1),
(255, 15, 'Me desanimo bastante ante los fracasos', 0, 1, 3, 1, 1),
(256, 16, 'Me gusta dirigir y hacer que las cosas funcionen', 0, 1, 1, 1, 1),
(257, 17, 'Busco el aplauso y la alabanza de los demás', 0, 1, 2, 1, 1),
(258, 18, 'Mi método preferido de aprendizaje es hacerlo yo mismo en lugar de mirar a los demás o leer sobre ello en un libro.', 0, 1, 3, 1, 1),
(259, 19, 'Generalmente no necesito consejo de nadie', 0, 1, 1, 1, 1),
(260, 20, 'Cuando tengo razón soy bastante duro e inflexible', 0, 1, 2, 1, 1),
(261, 1, 'Me esfuerzo por demostrar que tengo razón, aunque para ello tenga que luchar ', 0, 1, 1, 2, 1),
(262, 2, 'Procuro dar buena imagen de mi', 0, 1, 2, 2, 1),
(263, 3, 'Casi siempre tengo algunos proyectos en movimiento', 0, 1, 3, 2, 1),
(264, 4, 'Me siento bien dando instrucciones a los demás', 0, 1, 1, 2, 1),
(265, 5, 'En general manifiesto impaciencia por terminar las tareas o trabajos', 0, 1, 2, 2, 1),
(266, 6, 'Tengo una tendencia a postergar. A veces incluso aplazar mis prioridades. Esto hace que sea muy difícil terminar un proyecto', 0, 1, 3, 2, 1),
(267, 7, 'Soy una persona a la que se le puede otorgar confianza', 0, 1, 1, 2, 1),
(268, 8, 'Evito las discusiones que no llevan a nada', 0, 1, 2, 2, 1),
(269, 9, 'Mis amigos me hacen cambiar de opinión fácilmente', 0, 1, 3, 2, 1),
(270, 10, 'Creo que sé cómo tratar y llevar adecuadamente a la gente', 0, 1, 1, 2, 1),
(271, 11, 'Para mí son muy importantes mis obligaciones académicas y con las personas', 0, 1, 2, 2, 1),
(272, 12, 'Hacer algo lo suficientemente bueno y terminarlo es aún mejor que hacer algo grande pero nunca completarlo', 0, 1, 3, 2, 1),
(273, 13, 'Por lo general dudo bastante antes de hacer las cosas', 0, 1, 1, 2, 1),
(274, 14, 'Me gusta ir a reuniones y fiestas donde hay mucha gente', 0, 1, 2, 2, 1),
(275, 15, 'Abandono con facilidad las tareas cuando me encuentro con ciertos problemas', 0, 1, 3, 2, 1),
(276, 16, 'En las discusiones de grupo suelo llevar la iniciativa', 0, 1, 1, 2, 1),
(277, 17, 'Me cuesta entablar conversación con la gente', 0, 1, 2, 2, 1),
(278, 18, 'Me hundo con facilidad ante las adversidades', 0, 1, 3, 2, 1),
(279, 19, 'Generalmente hago las cosas a mi manera', 0, 1, 1, 2, 1),
(280, 20, 'Cuando alguien está diciendo tonterías, suelo interrumpirle y le hago callar', 0, 1, 2, 2, 1),
(281, 1, 'Me siento bien dando instrucciones a los demás', 0, 1, 1, 3, 1),
(282, 2, 'Me gustaría que los demás tuvieran otra opinión de mi', 0, 1, 2, 3, 1),
(283, 3, 'Rara vez siento el impulso creativo de hacer algo. Me gustaría estar más contento viendo la televisión o jugando videojuegos', 0, 1, 3, 3, 1),
(284, 4, 'Puedo presumir de qué, cuando tomo una decisión, nada ni nadie puede hacerme cambiar de opinión', 0, 1, 1, 3, 1),
(285, 5, 'Sé aceptar bien las críticas de los demás', 0, 1, 2, 3, 1),
(286, 6, 'Practico alguna forma de auto reflexión cada semana, ya sea meditación, soñar despierto, o simplemente sentarse y contemplar.', 0, 1, 3, 3, 1),
(287, 7, 'Cualquier excusa me es buena para abandonar lo que estoy haciendo', 0, 1, 1, 3, 1),
(288, 8, 'Me resulta fácil tomar parte en las discusiones o conversaciones de grupo', 0, 1, 2, 3, 1),
(289, 9, 'No me gusta mover el barco. Por lo general voy junto con el grupo, incluso si tengo una opinión diferente', 0, 1, 3, 3, 1),
(290, 10, 'Me gusta imponer a mis amigos los lugares a donde ir', 0, 1, 1, 3, 1),
(291, 11, 'Mi modo de hacer las cosas suele ser diferente a los demás', 0, 1, 2, 3, 1),
(292, 12, 'Es importante obtener los detalles correctos. El menor matiz puede causar el mayor impacto.', 0, 1, 3, 3, 1),
(293, 13, 'Me enfado mucho cuando estoy jugando y pierdo', 0, 1, 1, 3, 1),
(294, 14, 'Soy sincero o agresivo cuando debo serlo, y también diplomático cuando debo serlo', 0, 1, 2, 3, 1),
(295, 15, 'El concepto de aburrimiento me es ajeno. ¿Cómo se puede aburrirse en un universo infinito?', 0, 1, 3, 3, 1),
(296, 16, 'A veces soy autoritario y dominante', 0, 1, 1, 3, 1),
(297, 17, 'A menudo critico el modo de actuar y los errores de los demás', 0, 1, 2, 3, 1),
(298, 18, 'Simplemente no tengo la paciencia de hacer el mejor trabajo posible. Mi lema es lo \'suficientemente bueno\'', 0, 1, 3, 3, 1),
(299, 19, 'Mis problemas prefiero solucionarlos yo', 0, 1, 1, 3, 1),
(300, 20, 'Suelo tener en cuenta la opinión de los demás', 0, 1, 2, 3, 1),
(301, 1, 'A veces me inquieta asistir a reuniones sociales o conocer gente nueva', 0, 1, 1, 4, 1),
(302, 2, 'Cuando tengo problemas, prefiero tener a alguien a mi lado', 0, 1, 2, 4, 1),
(303, 3, 'Dejaré que otras personas decidan lo que significa mi trabajo. Sólo quiero sacarlo', 0, 1, 3, 4, 1),
(304, 4, 'Me encanta relacionarme con la gente', 0, 1, 1, 4, 1),
(305, 5, 'Me siento sobresaltado cuando tengo que hablar en público ', 0, 1, 2, 4, 1),
(306, 6, 'Trabajo muy duro en mi arte. Puse el tiempo, incluso cuando no me siento inspirado.', 0, 1, 3, 4, 1),
(307, 7, 'Cuando alguien se incorpora a un grupo de gente, lo acepto y entablo conversación con él/ella', 0, 1, 1, 4, 1),
(308, 8, 'Intento demostrar que sé o entiendo de todo', 0, 1, 2, 4, 1),
(309, 9, 'No me gusta mover el barco. Por lo general voy junto con el grupo, incluso si tengo una opinión diferente.', 0, 1, 3, 4, 1),
(310, 10, 'Me gusta participar en las conversaciones de grupos', 0, 1, 1, 4, 1),
(311, 11, 'Le doy una solución adecuada a la mayoría de los problemas que se me presentan', 0, 1, 2, 4, 1),
(312, 12, '“El arte está en cómo otros me ven o se ven.”', 0, 1, 3, 4, 1),
(313, 13, 'Me gusta más trabajar en equipo que solo', 0, 1, 1, 4, 1),
(314, 14, 'A veces tengo dificultades para concentrarme en mis tareas', 0, 1, 2, 4, 1),
(315, 15, '“Hacer o no hacer. Aquí no hay intentos.”', 0, 1, 3, 4, 1),
(316, 16, 'Cuando una persona me ha hecho algo, no le hablo o evito encontrarme con ella', 0, 1, 1, 4, 1),
(317, 17, 'Me cuesta bastante comenzar una conversación con desconocidos', 0, 1, 2, 4, 1),
(318, 18, 'Me gusta tomar riesgos (emocionales o profesionales) cuando creo. Las apuestas deben ser altas cuando se hace algo de importancia.', 0, 1, 3, 4, 1),
(319, 19, 'Cuando trabajo en grupo, prefiero hacerme cargo de la organización y desarrollo de tareas', 0, 1, 1, 4, 1),
(320, 20, 'Cuando las dificultades se me amontonan, me desconcierto y no sé qué hacer', 0, 1, 2, 4, 1),
(321, 21, 'Soy una persona reservada y retraída', 0, 2, 2, 1, 1),
(322, 21, 'Empiezo cosas que luego no termino', 0, 2, 3, 2, 1),
(323, 21, 'Me gusta imponer mis opiniones a los demás', 0, 2, 1, 3, 1),
(324, 21, 'En general manifiesto un comportamiento distante de la gente', 0, 2, 2, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regional`
--

CREATE TABLE `regional` (
  `regional_id` int(11) NOT NULL,
  `regional_nombre` varchar(150) NOT NULL,
  `regional_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `regional`
--

INSERT INTO `regional` (`regional_id`, `regional_nombre`, `regional_estado`) VALUES
(1, 'Amazonas', 1),
(2, 'Antioquia', 1),
(3, 'Arauca', 1),
(4, 'Atlántico', 1),
(5, 'Bolívar', 1),
(6, 'Boyacá', 1),
(7, 'Caldas', 1),
(8, 'Caquetá', 1),
(9, 'Casanare', 1),
(10, 'Cauca', 1),
(11, 'Cesar', 1),
(12, 'Chocó', 1),
(13, 'Córdoba', 1),
(14, 'Cundinamarca', 1),
(15, 'Dirección General', 1),
(16, 'Distrito Capital', 1),
(17, 'Guainía', 1),
(18, 'Guajira', 1),
(19, 'Guaviare', 1),
(20, 'Huila', 1),
(21, 'Magdalena', 1),
(22, 'Meta', 1),
(23, 'Nariño', 1),
(24, 'Norte de Santander', 1),
(25, 'Putumayo', 1),
(26, 'Quindío', 1),
(27, 'Risaralda', 1),
(28, 'San Andrés', 1),
(29, 'Santander', 1),
(30, 'Sucre', 1),
(31, 'Tolina', 1),
(32, 'Valle', 1),
(33, 'Vaupés', 1),
(34, 'Vichada', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `respuestas_id` int(11) NOT NULL,
  `respuestas_item` text NOT NULL,
  `respuestas_correct` tinyint(4) NOT NULL,
  `preguntas_preguntas_id` int(11) NOT NULL,
  `respuestas_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`respuestas_id`, `respuestas_item`, `respuestas_correct`, `preguntas_preguntas_id`, `respuestas_estado`) VALUES
(1, 'VERDADERO', 1, 241, 1),
(2, 'FALSO', 0, 241, 1),
(3, 'VERDADERO', 0, 242, 1),
(4, 'FALSO', 1, 242, 1),
(5, 'VERDADERO', 0, 243, 1),
(6, 'FALSO', 1, 243, 1),
(7, 'VERDADERO', 1, 244, 1),
(8, 'FALSO', 0, 244, 1),
(9, 'VERDADERO', 0, 245, 1),
(10, 'FALSO', 1, 245, 1),
(11, 'VERDADERO', 1, 246, 1),
(12, 'FALSO', 0, 246, 1),
(13, 'VERDADERO', 0, 247, 1),
(14, 'FALSO', 1, 247, 1),
(15, 'VERDADERO', 1, 248, 1),
(16, 'FALSO', 0, 248, 1),
(17, 'VERDADERO', 0, 249, 1),
(18, 'FALSO', 1, 249, 1),
(19, 'VERDADERO', 1, 250, 1),
(20, 'FALSO', 0, 250, 1),
(21, 'VERDADERO', 1, 251, 1),
(22, 'FALSO', 0, 251, 1),
(23, 'VERDADERO', 1, 252, 1),
(24, 'FALSO', 0, 252, 1),
(25, 'VERDADERO', 0, 253, 1),
(26, 'FALSO', 1, 253, 1),
(27, 'VERDADERO', 1, 254, 1),
(28, 'FALSO', 0, 254, 1),
(29, 'VERDADERO', 0, 255, 1),
(30, 'FALSO', 1, 255, 1),
(31, 'VERDADERO', 1, 256, 1),
(32, 'FALSO', 0, 256, 1),
(33, 'VERDADERO', 0, 257, 1),
(34, 'FALSO', 1, 257, 1),
(35, 'VERDADERO', 1, 258, 1),
(36, 'FALSO', 0, 258, 1),
(37, 'VERDADERO', 0, 259, 1),
(38, 'FALSO', 1, 259, 1),
(39, 'VERDADERO', 0, 260, 1),
(40, 'FALSO', 1, 260, 1),
(41, 'VERDADERO', 1, 261, 1),
(42, 'FALSO', 0, 261, 1),
(43, 'VERDADERO', 1, 262, 1),
(44, 'FALSO', 0, 262, 1),
(45, 'VERDADERO', 1, 263, 1),
(46, 'FALSO', 0, 263, 1),
(47, 'VERDADERO', 1, 264, 1),
(48, 'FALSO', 0, 264, 1),
(49, 'VERDADERO', 0, 265, 1),
(50, 'FALSO', 1, 265, 1),
(51, 'VERDADERO', 0, 266, 1),
(52, 'FALSO', 1, 266, 1),
(53, 'VERDADERO', 1, 267, 1),
(54, 'FALSO', 0, 267, 1),
(55, 'VERDADERO', 1, 268, 1),
(56, 'FALSO', 0, 268, 1),
(57, 'VERDADERO', 0, 269, 1),
(58, 'FALSO', 1, 269, 1),
(59, 'VERDADERO', 1, 270, 1),
(60, 'FALSO', 0, 270, 1),
(61, 'VERDADERO', 1, 271, 1),
(62, 'FALSO', 0, 271, 1),
(63, 'VERDADERO', 1, 272, 1),
(64, 'FALSO', 0, 272, 1),
(65, 'VERDADERO', 0, 273, 1),
(66, 'FALSO', 1, 273, 1),
(67, 'VERDADERO', 1, 274, 1),
(68, 'FALSO', 0, 274, 1),
(69, 'VERDADERO', 0, 275, 1),
(70, 'FALSO', 1, 275, 1),
(71, 'VERDADERO', 1, 276, 1),
(72, 'FALSO', 0, 276, 1),
(73, 'VERDADERO', 0, 277, 1),
(74, 'FALSO', 1, 277, 1),
(75, 'VERDADERO', 0, 278, 1),
(76, 'FALSO', 1, 278, 1),
(77, 'VERDADERO', 1, 279, 1),
(78, 'FALSO', 0, 279, 1),
(79, 'VERDADERO', 0, 280, 1),
(80, 'FALSO', 1, 280, 1),
(81, 'VERDADERO', 1, 281, 1),
(82, 'FALSO', 0, 281, 1),
(83, 'VERDADERO', 0, 282, 1),
(84, 'FALSO', 1, 282, 1),
(85, 'VERDADERO', 0, 283, 1),
(86, 'FALSO', 1, 283, 1),
(87, 'VERDADERO', 0, 284, 1),
(88, 'FALSO', 1, 284, 1),
(89, 'VERDADERO', 1, 285, 1),
(90, 'FALSO', 0, 285, 1),
(91, 'VERDADERO', 1, 286, 1),
(92, 'FALSO', 0, 286, 1),
(93, 'VERDADERO', 0, 287, 1),
(94, 'FALSO', 1, 287, 1),
(95, 'VERDADERO', 1, 288, 1),
(96, 'FALSO', 0, 288, 1),
(97, 'VERDADERO', 0, 289, 1),
(98, 'FALSO', 1, 289, 1),
(99, 'VERDADERO', 0, 290, 1),
(100, 'FALSO', 1, 290, 1),
(101, 'VERDADERO', 1, 291, 1),
(102, 'FALSO', 0, 291, 1),
(103, 'VERDADERO', 1, 292, 1),
(104, 'FALSO', 0, 292, 1),
(105, 'VERDADERO', 0, 293, 1),
(106, 'FALSO', 1, 293, 1),
(107, 'VERDADERO', 1, 294, 1),
(108, 'FALSO', 0, 294, 1),
(109, 'VERDADERO', 1, 295, 1),
(110, 'FALSO', 0, 295, 1),
(111, 'VERDADERO', 0, 296, 1),
(112, 'FALSO', 1, 296, 1),
(113, 'VERDADERO', 0, 297, 1),
(114, 'FALSO', 1, 297, 1),
(115, 'VERDADERO', 0, 298, 1),
(116, 'FALSO', 1, 298, 1),
(117, 'VERDADERO', 0, 299, 1),
(118, 'FALSO', 1, 299, 1),
(119, 'VERDADERO', 1, 300, 1),
(120, 'FALSO', 0, 300, 1),
(121, 'VERDADERO', 0, 301, 1),
(122, 'FALSO', 1, 301, 1),
(123, 'VERDADERO', 1, 302, 1),
(124, 'FALSO', 0, 302, 1),
(125, 'VERDADERO', 0, 303, 1),
(126, 'FALSO', 1, 303, 1),
(127, 'VERDADERO', 1, 304, 1),
(128, 'FALSO', 0, 304, 1),
(129, 'VERDADERO', 0, 305, 1),
(130, 'FALSO', 1, 305, 1),
(131, 'VERDADERO', 1, 306, 1),
(132, 'FALSO', 0, 306, 1),
(133, 'VERDADERO', 1, 307, 1),
(134, 'FALSO', 0, 307, 1),
(135, 'VERDADERO', 0, 308, 1),
(136, 'FALSO', 1, 308, 1),
(137, 'VERDADERO', 0, 309, 1),
(138, 'FALSO', 1, 309, 1),
(139, 'VERDADERO', 1, 310, 1),
(140, 'FALSO', 0, 310, 1),
(141, 'VERDADERO', 1, 311, 1),
(142, 'FALSO', 0, 311, 1),
(143, 'VERDADERO', 1, 312, 1),
(144, 'FALSO', 0, 312, 1),
(145, 'VERDADERO', 1, 313, 1),
(146, 'FALSO', 0, 313, 1),
(147, 'VERDADERO', 0, 314, 1),
(148, 'FALSO', 1, 314, 1),
(149, 'VERDADERO', 0, 315, 1),
(150, 'FALSO', 1, 315, 1),
(151, 'VERDADERO', 0, 316, 1),
(152, 'FALSO', 1, 316, 1),
(153, 'VERDADERO', 0, 317, 1),
(154, 'FALSO', 1, 317, 1),
(155, 'VERDADERO', 1, 318, 1),
(156, 'FALSO', 0, 318, 1),
(157, 'VERDADERO', 1, 319, 1),
(158, 'FALSO', 0, 319, 1),
(159, 'VERDADERO', 0, 320, 1),
(160, 'FALSO', 1, 320, 1),
(161, 'VERDADERO', 0, 321, 1),
(162, 'FALSO', 1, 321, 1),
(163, 'VERDADERO', 0, 322, 1),
(164, 'FALSO', 1, 322, 1),
(165, 'VERDADERO', 0, 323, 1),
(166, 'FALSO', 1, 323, 1),
(167, 'VERDADERO', 0, 324, 1),
(168, 'FALSO', 1, 324, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `roles_id` int(11) NOT NULL,
  `roles_nombre` varchar(40) NOT NULL,
  `roles_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`roles_id`, `roles_nombre`, `roles_estado`) VALUES
(1, 'Super', 1),
(2, 'Jugador', 1),
(3, 'Admin', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE `test` (
  `test_id` int(11) NOT NULL,
  `test_item` varchar(255) NOT NULL,
  `test_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`test_id`, `test_item`, `test_estado`) VALUES
(1, 'Pre-Test', 1),
(2, 'Test 1', 1),
(3, 'Test 2', 1),
(4, 'Post-Test', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_perfil`
--

CREATE TABLE `tipo_perfil` (
  `tipo_perfil_id` int(11) NOT NULL,
  `tipo_perfil_nombre` varchar(80) NOT NULL COMMENT 'SENA / EMPRESA',
  `tipo_perfil_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_perfil`
--

INSERT INTO `tipo_perfil` (`tipo_perfil_id`, `tipo_perfil_nombre`, `tipo_perfil_estado`) VALUES
(1, 'SENA', 1),
(2, 'EMPRESA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pregunta`
--

CREATE TABLE `tipo_pregunta` (
  `tipo_pregunta_id` int(11) NOT NULL,
  `tipo_preguna_item` varchar(45) NOT NULL,
  `tipo_pregunta_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_pregunta`
--

INSERT INTO `tipo_pregunta` (`tipo_pregunta_id`, `tipo_preguna_item`, `tipo_pregunta_estado`) VALUES
(1, 'Normal', 1),
(2, 'Control', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `usuario_nombres` varchar(80) NOT NULL,
  `usuario_apellidos` varchar(80) NOT NULL,
  `usuario_edad` varchar(20) NOT NULL,
  `usuario_tipo_doc` varchar(20) NOT NULL,
  `usuario_documento` varchar(20) NOT NULL,
  `usuario_genero` varchar(10) NOT NULL,
  `usuario_email` varchar(90) NOT NULL,
  `usuario_password` text DEFAULT NULL,
  `usuario_telefono` varchar(20) NOT NULL,
  `usuario_steam` varchar(255) NOT NULL,
  `usuario_nivel` int(11) NOT NULL,
  `usuario_monedas` varchar(255) NOT NULL,
  `usuario_gemas` varchar(255) NOT NULL,
  `usuario_items_avatar` text NOT NULL,
  `usuario_items_closet` text NOT NULL,
  `usuario_programa` varchar(100) DEFAULT NULL,
  `usuario_ficha` varchar(45) DEFAULT NULL,
  `usuario_empresa` varchar(150) DEFAULT NULL,
  `usuario_nit` varchar(20) DEFAULT NULL,
  `usuario_emp_telefono` varchar(20) DEFAULT NULL,
  `usuario_estado` varchar(45) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `roles_roles_id` int(11) NOT NULL,
  `tipo_perfil_tipo_perfil_id` int(11) NOT NULL,
  `centro_centro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario_nombres`, `usuario_apellidos`, `usuario_edad`, `usuario_tipo_doc`, `usuario_documento`, `usuario_genero`, `usuario_email`, `usuario_password`, `usuario_telefono`, `usuario_steam`, `usuario_nivel`, `usuario_monedas`, `usuario_gemas`, `usuario_items_avatar`, `usuario_items_closet`, `usuario_programa`, `usuario_ficha`, `usuario_empresa`, `usuario_nit`, `usuario_emp_telefono`, `usuario_estado`, `created`, `roles_roles_id`, `tipo_perfil_tipo_perfil_id`, `centro_centro_id`) VALUES
(1, 'Super', 'Admin', '30', 'C.C', '1111111111', 'M', 'super@admin.com', '$2y$10$xoBgA4pJN.db5QN01GVp8.Iw/6DRWh9WJOwfKj9rUjmAm69U53cSy', '0000000000', '0', 1, '0', '0', '{}', '{}', NULL, NULL, NULL, NULL, NULL, '1', '2021-11-17 04:29:56', 1, 1, 1),
(23, 'Personal', 'Administrador', '0', '0', '0', '0', 'admin@admin.com', '$2y$10$LgKbZ5KefxDaJ22ns0KzTOPDhfOL/m6VN7gi2slbrLiG.Ax.NjiT.', '0', '0', 1, '0', '0', '0', '0', NULL, NULL, NULL, NULL, NULL, '1', '2021-11-27 18:15:44', 3, 1, 1),
(28, 'Johanna', 'Zapata', '0', '0', '0', '0', 'direccion@infinitymind.co', '$2y$10$eLm.zW77u9wv2vmfnLKZRu/UOQIeyoGtht3E9I0eE4bMB8/0Yu0f6', '0', '0', 1, '0', '0', '0', '0', NULL, NULL, NULL, NULL, NULL, '1', '2021-11-28 18:51:34', 3, 1, 1),
(32, 'Mauricio', 'Lopez', '36', 'Documento', '451164415645', 'Hombre', 'sertv@drtgy.dthuyb', NULL, '451645', '76561197995833783', 3, '15000000', '15005', '[{\"Belt\":{\"id\":8,\"material\":1}},{\"Boots\":{\"id\":10,\"material\":1}},{\"Chest\":{\"id\":93,\"material\":1}},{\"Gloves\":{\"id\":0,\"material\":0}},{\"Helm\":{\"id\":0,\"material\":0}},{\"Pants\":{\"id\":7,\"material\":4}},{\"Shoulders\":{\"id\":18,\"material\":0}},{\"Eye\":{\"id\":87,\"material\":1}},{\"Hair\":{\"id\":85,\"material\":1}},{\"Arma\":{\"id\":57,\"material\":0}},{\"Escudo\":{\"id\":0,\"material\":0}},{\"extra\":{\"id\":0,\"material\":0}}]', '[{\"id\":8,\"material\":1},{\"id\":10,\"material\":1},{\"id\":93,\"material\":1},{\"id\":5,\"material\":1},{\"id\":87,\"material\":1},{\"id\":85,\"material\":1},{\"id\":57,\"material\":0},{\"id\":18,\"material\":0},{\"id\":7,\"material\":4},{\"id\":30,\"material\":0}]', '1111', '1111', 'Impacta Games', '4186476', '3144506115', '1', '2021-11-28 23:32:06', 2, 2, 1),
(33, 'Andres', 'Molina', '39', 'Documento', '11228076', 'Hombre', 'envialibre@gmail.com', NULL, '3133373348', '76561199098048733', 1, '15700', '0', '[{\"Belt\":{\"id\":8,\"material\":1}},{\"Boots\":{\"id\":12,\"material\":3}},{\"Chest\":{\"id\":93,\"material\":1}},{\"Gloves\":{\"id\":0,\"material\":0}},{\"Helm\":{\"id\":30,\"material\":0}},{\"Pants\":{\"id\":5,\"material\":1}},{\"Shoulders\":{\"id\":18,\"material\":0}},{\"Eye\":{\"id\":87,\"material\":1}},{\"Hair\":{\"id\":0,\"material\":0}},{\"Arma\":{\"id\":0,\"material\":0}},{\"Escudo\":{\"id\":0,\"material\":0}},{\"extra\":{\"id\":0,\"material\":0}}]', '[{\"id\":8,\"material\":1},{\"id\":10,\"material\":1},{\"id\":93,\"material\":1},{\"id\":5,\"material\":1},{\"id\":87,\"material\":1},{\"id\":85,\"material\":1},{\"id\":30,\"material\":0},{\"id\":12,\"material\":3},{\"id\":18,\"material\":0}]', '1111', '1111', 'pruebas', '123456789', '123456789', '1', '2021-11-28 23:47:01', 2, 2, 1),
(34, 'DIANA CAROLINA', 'ZAPATA ROMERO', '34', 'Documento', '123456789', 'Mujer', 'DIARTGRAPHICS@GMAIL.COM', NULL, '45789658', '76561199170993401', 1, '15500', '0', '[{\"Belt\":{\"id\":8,\"material\":1}},{\"Boots\":{\"id\":10,\"material\":1}},{\"Chest\":{\"id\":19,\"material\":1}},{\"Gloves\":{\"id\":0,\"material\":0}},{\"Helm\":{\"id\":0,\"material\":0}},{\"Pants\":{\"id\":5,\"material\":1}},{\"Shoulders\":{\"id\":15,\"material\":0}},{\"Eye\":{\"id\":87,\"material\":1}},{\"Hair\":{\"id\":86,\"material\":1}},{\"Arma\":{\"id\":0,\"material\":0}},{\"Escudo\":{\"id\":0,\"material\":0}},{\"extra\":{\"id\":0,\"material\":0}}]', '[{\"id\":8,\"material\":1},{\"id\":10,\"material\":1},{\"id\":19,\"material\":1},{\"id\":5,\"material\":1},{\"id\":87,\"material\":1},{\"id\":86,\"material\":1},{\"id\":3,\"material\":0},{\"id\":15,\"material\":0},{\"id\":13,\"material\":0}]', '1111', '1111', 'PEPITO', '12456878522', '214587952', '1', '2021-11-28 23:47:40', 2, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_juegos_nivel`
--

CREATE TABLE `usuario_juegos_nivel` (
  `usuario_usuario_id` int(11) NOT NULL,
  `juegos_nivel_juegos_juegos_id` int(11) NOT NULL,
  `juegos_nivel_nivel_nivel_id` int(11) NOT NULL,
  `usuario_juegos_nivel_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_juegos_nivel`
--

INSERT INTO `usuario_juegos_nivel` (`usuario_usuario_id`, `juegos_nivel_juegos_juegos_id`, `juegos_nivel_nivel_nivel_id`, `usuario_juegos_nivel_estado`) VALUES
(32, 1, 1, 1),
(32, 1, 2, 1),
(32, 1, 3, 1),
(32, 2, 1, 0),
(32, 2, 2, 0),
(32, 2, 3, 0),
(32, 3, 1, 0),
(32, 3, 2, 0),
(32, 3, 3, 0),
(32, 4, 1, 0),
(32, 4, 2, 0),
(32, 4, 3, 0),
(32, 5, 1, 1),
(32, 5, 2, 0),
(32, 5, 3, 1),
(33, 1, 1, 1),
(33, 1, 2, 0),
(33, 1, 3, 0),
(33, 2, 1, 1),
(33, 2, 2, 0),
(33, 2, 3, 0),
(33, 3, 1, 1),
(33, 3, 2, 0),
(33, 3, 3, 0),
(33, 4, 1, 1),
(33, 4, 2, 0),
(33, 4, 3, 0),
(33, 5, 1, 1),
(33, 5, 2, 0),
(33, 5, 3, 0),
(34, 1, 1, 1),
(34, 1, 2, 0),
(34, 1, 3, 0),
(34, 2, 1, 1),
(34, 2, 2, 0),
(34, 2, 3, 0),
(34, 3, 1, 1),
(34, 3, 2, 0),
(34, 3, 3, 0),
(34, 4, 1, 1),
(34, 4, 2, 0),
(34, 4, 3, 0),
(34, 5, 1, 1),
(34, 5, 2, 0),
(34, 5, 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_respuestas`
--

CREATE TABLE `usuario_respuestas` (
  `usuario_respuestas_id` int(11) NOT NULL,
  `usuario_usuario_id` int(11) NOT NULL,
  `respuestas_respuestas_id` int(11) NOT NULL,
  `preguntas_preguntas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_respuestas`
--

INSERT INTO `usuario_respuestas` (`usuario_respuestas_id`, `usuario_usuario_id`, `respuestas_respuestas_id`, `preguntas_preguntas_id`) VALUES
(873, 33, 1, 241),
(874, 33, 3, 242),
(875, 33, 6, 243),
(876, 33, 7, 244),
(877, 33, 10, 245),
(878, 33, 11, 246),
(879, 33, 14, 247),
(880, 33, 15, 248),
(881, 33, 18, 249),
(882, 33, 19, 250),
(883, 33, 22, 251),
(884, 33, 23, 252),
(885, 33, 26, 253),
(886, 33, 27, 254),
(887, 33, 30, 255),
(888, 33, 31, 256),
(889, 33, 34, 257),
(890, 33, 35, 258),
(891, 33, 38, 259),
(892, 33, 39, 260),
(893, 33, 161, 321),
(894, 34, 1, 241),
(895, 34, 4, 242),
(896, 34, 6, 243),
(897, 34, 7, 244),
(898, 34, 10, 245),
(899, 34, 11, 246),
(900, 34, 14, 247),
(901, 34, 15, 248),
(902, 34, 18, 249),
(903, 34, 19, 250),
(904, 34, 21, 251),
(905, 34, 24, 252),
(906, 34, 26, 253),
(907, 34, 27, 254),
(908, 34, 30, 255),
(909, 34, 31, 256),
(910, 34, 34, 257),
(911, 34, 35, 258),
(912, 34, 38, 259),
(913, 34, 40, 260),
(914, 34, 162, 321),
(915, 32, 1, 241),
(916, 32, 4, 242),
(917, 32, 6, 243),
(918, 32, 7, 244),
(919, 32, 10, 245),
(920, 32, 12, 246),
(921, 32, 14, 247),
(922, 32, 16, 248),
(923, 32, 18, 249),
(924, 32, 19, 250),
(925, 32, 22, 251),
(926, 32, 24, 252),
(927, 32, 26, 253),
(928, 32, 28, 254),
(929, 32, 29, 255),
(930, 32, 32, 256),
(931, 32, 34, 257),
(932, 32, 36, 258),
(933, 32, 38, 259),
(934, 32, 40, 260),
(935, 32, 161, 321);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_test`
--

CREATE TABLE `usuario_test` (
  `usuario_usuario_id` int(11) NOT NULL,
  `test_test_id` int(11) NOT NULL,
  `usuario_test_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_test`
--

INSERT INTO `usuario_test` (`usuario_usuario_id`, `test_test_id`, `usuario_test_estado`) VALUES
(32, 1, 1),
(32, 2, 0),
(32, 3, 0),
(32, 4, 0),
(33, 1, 1),
(33, 2, 0),
(33, 3, 0),
(33, 4, 0),
(34, 1, 1),
(34, 2, 0),
(34, 3, 0),
(34, 4, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `centro`
--
ALTER TABLE `centro`
  ADD PRIMARY KEY (`centro_id`),
  ADD KEY `fk_centro_regional1_idx` (`regional_regional_id`);

--
-- Indices de la tabla `habilidades`
--
ALTER TABLE `habilidades`
  ADD PRIMARY KEY (`habilidades_id`);

--
-- Indices de la tabla `juegos`
--
ALTER TABLE `juegos`
  ADD PRIMARY KEY (`juegos_id`);

--
-- Indices de la tabla `juegos_nivel`
--
ALTER TABLE `juegos_nivel`
  ADD PRIMARY KEY (`juegos_juegos_id`,`nivel_nivel_id`),
  ADD KEY `fk_juegos_has_nivel_nivel1_idx` (`nivel_nivel_id`),
  ADD KEY `fk_juegos_has_nivel_juegos1_idx` (`juegos_juegos_id`);

--
-- Indices de la tabla `nivel`
--
ALTER TABLE `nivel`
  ADD PRIMARY KEY (`nivel_id`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`preguntas_id`),
  ADD KEY `fk_preguntas_tipo_pregunta1_idx` (`tipo_pregunta_tipo_pregunta_id`),
  ADD KEY `fk_preguntas_habilidades1_idx` (`habilidades_habilidades_id`),
  ADD KEY `fk_preguntas_test1_idx` (`test_test_id`);

--
-- Indices de la tabla `regional`
--
ALTER TABLE `regional`
  ADD PRIMARY KEY (`regional_id`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`respuestas_id`),
  ADD KEY `fk_respuestas_preguntas1_idx` (`preguntas_preguntas_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`roles_id`);

--
-- Indices de la tabla `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`test_id`);

--
-- Indices de la tabla `tipo_perfil`
--
ALTER TABLE `tipo_perfil`
  ADD PRIMARY KEY (`tipo_perfil_id`);

--
-- Indices de la tabla `tipo_pregunta`
--
ALTER TABLE `tipo_pregunta`
  ADD PRIMARY KEY (`tipo_pregunta_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`),
  ADD KEY `fk_usuario_roles_idx` (`roles_roles_id`),
  ADD KEY `fk_usuario_tipo_perfil1_idx` (`tipo_perfil_tipo_perfil_id`),
  ADD KEY `fk_usuario_centro1_idx` (`centro_centro_id`);

--
-- Indices de la tabla `usuario_juegos_nivel`
--
ALTER TABLE `usuario_juegos_nivel`
  ADD PRIMARY KEY (`usuario_usuario_id`,`juegos_nivel_juegos_juegos_id`,`juegos_nivel_nivel_nivel_id`),
  ADD KEY `fk_usuario_has_juegos_nivel_juegos_nivel1_idx` (`juegos_nivel_juegos_juegos_id`,`juegos_nivel_nivel_nivel_id`),
  ADD KEY `fk_usuario_has_juegos_nivel_usuario1_idx` (`usuario_usuario_id`);

--
-- Indices de la tabla `usuario_respuestas`
--
ALTER TABLE `usuario_respuestas`
  ADD PRIMARY KEY (`usuario_respuestas_id`),
  ADD KEY `fk_usuario_has_respuestas_respuestas1_idx` (`respuestas_respuestas_id`),
  ADD KEY `fk_usuario_has_respuestas_usuario1_idx` (`usuario_usuario_id`),
  ADD KEY `fk_usuario_respuestas_preguntas1_idx` (`preguntas_preguntas_id`);

--
-- Indices de la tabla `usuario_test`
--
ALTER TABLE `usuario_test`
  ADD PRIMARY KEY (`usuario_usuario_id`,`test_test_id`),
  ADD KEY `fk_usuario_has_test_test1_idx` (`test_test_id`),
  ADD KEY `fk_usuario_has_test_usuario1_idx` (`usuario_usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `centro`
--
ALTER TABLE `centro`
  MODIFY `centro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT de la tabla `habilidades`
--
ALTER TABLE `habilidades`
  MODIFY `habilidades_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `juegos`
--
ALTER TABLE `juegos`
  MODIFY `juegos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `nivel`
--
ALTER TABLE `nivel`
  MODIFY `nivel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `preguntas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=325;

--
-- AUTO_INCREMENT de la tabla `regional`
--
ALTER TABLE `regional`
  MODIFY `regional_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `respuestas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `roles_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `test`
--
ALTER TABLE `test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipo_perfil`
--
ALTER TABLE `tipo_perfil`
  MODIFY `tipo_perfil_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_pregunta`
--
ALTER TABLE `tipo_pregunta`
  MODIFY `tipo_pregunta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `usuario_respuestas`
--
ALTER TABLE `usuario_respuestas`
  MODIFY `usuario_respuestas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=936;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `centro`
--
ALTER TABLE `centro`
  ADD CONSTRAINT `fk_centro_regional1` FOREIGN KEY (`regional_regional_id`) REFERENCES `regional` (`regional_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `juegos_nivel`
--
ALTER TABLE `juegos_nivel`
  ADD CONSTRAINT `fk_juegos_has_nivel_juegos1` FOREIGN KEY (`juegos_juegos_id`) REFERENCES `juegos` (`juegos_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_juegos_has_nivel_nivel1` FOREIGN KEY (`nivel_nivel_id`) REFERENCES `nivel` (`nivel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD CONSTRAINT `fk_preguntas_habilidades1` FOREIGN KEY (`habilidades_habilidades_id`) REFERENCES `habilidades` (`habilidades_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_preguntas_test1` FOREIGN KEY (`test_test_id`) REFERENCES `test` (`test_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_preguntas_tipo_pregunta1` FOREIGN KEY (`tipo_pregunta_tipo_pregunta_id`) REFERENCES `tipo_pregunta` (`tipo_pregunta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD CONSTRAINT `fk_respuestas_preguntas1` FOREIGN KEY (`preguntas_preguntas_id`) REFERENCES `preguntas` (`preguntas_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_centro1` FOREIGN KEY (`centro_centro_id`) REFERENCES `centro` (`centro_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_roles` FOREIGN KEY (`roles_roles_id`) REFERENCES `roles` (`roles_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_tipo_perfil1` FOREIGN KEY (`tipo_perfil_tipo_perfil_id`) REFERENCES `tipo_perfil` (`tipo_perfil_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_juegos_nivel`
--
ALTER TABLE `usuario_juegos_nivel`
  ADD CONSTRAINT `fk_usuario_has_juegos_nivel_juegos_nivel1` FOREIGN KEY (`juegos_nivel_juegos_juegos_id`,`juegos_nivel_nivel_nivel_id`) REFERENCES `juegos_nivel` (`juegos_juegos_id`, `nivel_nivel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_juegos_nivel_usuario1` FOREIGN KEY (`usuario_usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_respuestas`
--
ALTER TABLE `usuario_respuestas`
  ADD CONSTRAINT `fk_usuario_has_respuestas_respuestas1` FOREIGN KEY (`respuestas_respuestas_id`) REFERENCES `respuestas` (`respuestas_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_respuestas_usuario1` FOREIGN KEY (`usuario_usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_respuestas_preguntas1` FOREIGN KEY (`preguntas_preguntas_id`) REFERENCES `preguntas` (`preguntas_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_test`
--
ALTER TABLE `usuario_test`
  ADD CONSTRAINT `fk_usuario_has_test_test1` FOREIGN KEY (`test_test_id`) REFERENCES `test` (`test_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_test_usuario1` FOREIGN KEY (`usuario_usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
