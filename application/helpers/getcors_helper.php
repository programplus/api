<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_cors_api'))
{
	function get_cors_api()
	{
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
	}
}