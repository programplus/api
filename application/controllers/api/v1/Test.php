<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Test extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function obtener_post(){

        $test_id = $this->post('test_id');
        $status = false;
        $message = '';
        $result = array();

        if( $test_id ){

            $result = $this->Model->obtener_test( $test_id );
            
            if( $result ){
                
                $i = 0;

                foreach ($result as $key_test => $test) {
                    $result[$i]->preguntas = $this->Model->obtener_preguntas_test( $test->test_id );
                    $j = 0;
                    
                    if( $result[$i]->preguntas ){
                        foreach ($result[$i]->preguntas as $key_preg => $preg) {
                            $result[$i]->preguntas[$j]->respuestas = $this->Model->obtener_respuestas_test( $preg->pregunta_id );
                            $j++;
                        }
                    }

                    $i++;
                }

            }

            $status = true;
        }
        else{
            $message = 'Debe especificar el test para obtener las preguntas y respuestas pertenecientes a el';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $result
        ], RestController::HTTP_OK);
    }

    public function guardar_test_post(){

        $data = $this->post('data');

        $status = false;
        $message = '';
        $result = false;

        $habilidades = $this->Model->obtener_habilidades();

        if( $data['test_id'] && $data['steam_id'] && count($data['respuestas']) > 0){

            $usuario = $this->Model->obtener_usuario( NULL, $data['steam_id'], NULL);

                if( $usuario ){
                    //Registro respuestas y cambio de test a realizado
                    foreach ($data['respuestas'] as $key => $item) {

                        $estado_test = $this->Model->obtener_estado_test( $usuario->usuario_id, $data['test_id'] );

                        $aux = [
                            'usuario_usuario_id'        => $usuario->usuario_id,
                            'respuestas_respuestas_id'  => $item['respuesta_id'],
                            'preguntas_preguntas_id'    => $item['pregunta_id']
                        ];

                        $condition = [
                            'usuario_usuario_id'        => $usuario->usuario_id,
                            'preguntas_preguntas_id'    => $item['pregunta_id']
                        ];

                        $result = $estado_test === '1' ? $this->Model->update('usuario_respuestas', $aux, $condition) : $this->Model->insert( 'usuario_respuestas', $aux );
                    }

                    if( $result ){

                        $update_test = array(
                            'usuario_test_estado'   => 1
                        );

                        $condition = array(
                            'usuario_usuario_id'    => $usuario->usuario_id,
                            'test_test_id'          => $data['test_id']
                        );

                        $this->Model->update('usuario_test', $update_test, $condition);
                    }


                    // Calculo porcentaje

                    foreach ($habilidades as $key => $item) {
                        $habilidades[$key]->cant_preg = $this->Model->cantidad_habilidades_preguntas_test($data['test_id'], $item->habilidad_id );
                        $habilidades[$key]->cant_resp = 0;
                        $habilidades[$key]->percentage = 0;
                    }

                    $aux_resp = $this->Model->obtener_usuario_respuestas_test( $usuario->usuario_id, $data['test_id'], true );

                    foreach ($aux_resp as $key_aux => $item) {
                        
                        if( $item->tipo_pregunta == 1 && $item->respuestas_correct == 1 ){
                            
                            $index = $this->filter_habilidades( $habilidades, $item->habilidades_habilidades_id );
                            
                            $habilidades[$index]->cant_resp += 1;

                            $habilidades[$index]->percentage = round( (($habilidades[$index]->cant_resp * 100)/$habilidades[$index]->cant_preg) ) . '%';
                            //$total = array_sum( array_column($habilidades, 'cant_preg') );
                        }
                    }

                    $message = 'Hola, de acuerdo al test que acabas de responder tus habilidades blandas se puntúan de la siguiente manera:';

                    $status = true;
                }
                else{
                    $message = 'El ID de steam del jugador no coincide con los registros en la base de datos';
                }
        }
        else{
            $message = 'Debe especificar la data para almacenar la solución del test';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $habilidades
        ], RestController::HTTP_OK);
    }

    private function filter_habilidades( $array, $index ){
        
        $x = 0;

        foreach ($array as $key => $item) {
            if( $item->habilidad_id == $index ){
                $x = $key;
            }
        }

        return $x;
    }
}