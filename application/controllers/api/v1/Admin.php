<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Admin extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function obtener_usuarios_get(){

        $data = $this->Model->obtener_usuarios();

        if( $data ){
            $this->response([
                'status'    => true,
                'message'   => '',
                'data'      => $data
            ], RestController::HTTP_OK);
        }
        else{
            $this->response([
                'status'    => false,
                'message'   => 'Error al recuperar el listado de jugadores',
                'data'      => []
            ], RestController::HTTP_OK);
        }
        
    }

    public function obtener_test_usuario_get(){

        $usuarios = $this->Model->obtener_usuarios();
        $tests = $this->Model->obtener_tests();
        $preg_resp = array();

        foreach ($tests as $key => $test) {
            $habilidades = $this->Model->contar_pregs_habil_test( $test->test_id );
            array_push($preg_resp, $this->Model->obtener_preguntas_test( $test->test_id ));
        }

        foreach ($habilidades as $key => $item) {
            $habilidades[$key]['cant_resp'] = 0;
            $habilidades[$key]['percentage'] = 0;
        }

        $u = 0;
        foreach ($usuarios as $keyu => $usuario) {
            $usuarios[$u]['test'] = $this->Model->obtener_tests_usuarios( $usuario['id'] );
            $t = 0;
            foreach ($usuarios[$u]['test'] as $keyt => $test) {
                
                $usuarios[$u]['test'][$t]->respuestas = $this->Model->obtener_usuario_respuestas_test( $usuario['id'], $test->test_id, true );
                if( $usuarios[$u]['test'][$t]->respuestas ){
                    foreach ($usuarios[$u]['test'][$t]->respuestas as $keyr => $respuesta) {

                        if( $respuesta->tipo_pregunta == 1 && $respuesta->respuestas_correct == 1 ){

                            $index = $this->filter_habilidades( $habilidades, $respuesta->habilidades_habilidades_id );
                            $habilidades[$index]['cant_resp'] += 1;

                            $habilidades[$index]['percentage'] = round( (($habilidades[$index]['cant_resp'] * 100)/$habilidades[$index]['cant_preg']) ) . '%';
                            $habilidades[$index]['media'] = (int)round( (($habilidades[$index]['cant_resp'] * 100)/$habilidades[$index]['cant_preg']) );
                        }
                    }

                    $usuarios[$u]['test'][$t]->habilidades[] = $habilidades;

                    foreach ($habilidades as $key => $item) {
                        $habilidades[$key]['cant_resp'] = 0;
                        $habilidades[$key]['percentage'] = 0;
                        $habilidades[$key]['media'] = 0;
                    }
                }
                $t++;
            }
            $u++;
        }

        $this->response([
            'data'  => [
                'usuarios'  => $usuarios,
                'preg_resp'  => $preg_resp,
                'test'       => $tests,
                'habilidades'   => $habilidades
            ] 
        ], RestController::HTTP_OK);
    }

    public function obtener_test_get(){
        
        $test = $this->Model->obtener_tests();

        $this->response( $test, RestController::HTTP_OK );
    }

    public function crear_administradores_post(){

        $status = false;
        $message = '';

        $data = $this->post();

        $insert_data = array(
            'usuario_nombres'               => $data['nombres'],
            'usuario_apellidos'             => $data['apellidos'],
            'usuario_edad'                  => '0',
            'usuario_tipo_doc'              => '0',
            'usuario_documento'             => '0',
            'usuario_genero'                => '0',
            'usuario_email'                 => $data['email'],
            'usuario_password'              => password_hash($data['password'], PASSWORD_DEFAULT),
            'usuario_telefono'              => '0',
            'usuario_steam'                 => '0',
            'usuario_nivel'                 => '1',
            'usuario_monedas'               => '0',
            'usuario_gemas'                 => '0',
            'usuario_items_avatar'          => '0',
            'usuario_items_closet'          => '0',
            'usuario_estado'                => '1',
            'roles_roles_id'                => '3',
            'tipo_perfil_tipo_perfil_id'    => '1',
            'centro_centro_id'              => '1'
        );

        $result = $this->Model->insert('usuario', $insert_data);
        if( $result ){
            
            $status = true;
            $message = 'El administrador se creó correctamente';

        }
        else{
            $message = 'Ha ocurrido un error al momento de crear el administrador';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }

    public function obtener_administradores_get(){

        $data = $this->Model->get('usuario', array('roles_roles_id' => 3));

        if( $data ){
            $this->response([
                'data'  => $data
            ], RestController::HTTP_OK );
        }
        else{
            $this->response( [], RestController::HTTP_OK );
        }
    }

    public function obtener_detalle_usuario_get( $usuario_id ){

        $usuario = $this->Model->obtener_usuario( $usuario_id );
        $tests = $this->Model->obtener_tests();
        $preg_resp = array();

        foreach ($tests as $key => $test) {
            $habilidades = $this->Model->contar_pregs_habil_test( $test->test_id );
            array_push($preg_resp, $this->Model->obtener_preguntas_test( $test->test_id ));
        }

        foreach ($habilidades as $key => $item) {
            $habilidades[$key]['cant_resp'] = 0;
            $habilidades[$key]['percentage'] = 0;
        }

        $usuario->test = $this->Model->obtener_tests_usuarios( $usuario_id );
        $t = 0;
        foreach ($usuario->test as $keyt => $test) {
            
            $usuario->test[$t]->respuestas = $this->Model->obtener_usuario_respuestas_test( $usuario_id, $test->test_id, false );
            if( $usuario->test[$t]->respuestas ){
                foreach ($usuario->test[$t]->respuestas as $keyr => $respuesta) {

                    if( $respuesta->tipo_pregunta == 1 && $respuesta->respuestas_correct == 1 ){

                        $index = $this->filter_habilidades( $habilidades, $respuesta->habilidades_habilidades_id );
                        $habilidades[$index]['cant_resp'] += 1;

                        $habilidades[$index]['percentage'] = round( (($habilidades[$index]['cant_resp'] * 100)/$habilidades[$index]['cant_preg']) ) . '%';
                        $habilidades[$index]['media'] = (int)round( (($habilidades[$index]['cant_resp'] * 100)/$habilidades[$index]['cant_preg']) );
                    }
                }

                $usuario->test[$t]->habilidades[] = $habilidades;

                foreach ($habilidades as $key => $item) {
                    $habilidades[$key]['cant_resp'] = 0;
                    $habilidades[$key]['percentage'] = 0;
                    $habilidades[$key]['media'] = 0;
                }
            }
            $t++;
        }

        $this->response([
            'data'  => $usuario
        ], RestController::HTTP_OK);
    }

    public function change_password_admin_post(){

        $status = false;
        $message = '';

        $data = $this->post();

        $update = array(
            'usuario_password'  => password_hash($data['password'], PASSWORD_DEFAULT)
        );

        $condition = array(
            'usuario_id'    => $data['usuario_id']
        );

        $result = $this->Model->update('usuario', $update, $condition);

        if( $result ){
            $status = true;
            $message = 'La contraseña del usuario administrador, se modificó correctamente';
        }
        else{
            $message = 'Ha ocurrido un error, intentalo nuevamente';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }

    public function change_data_admin_post(){

        $status = false;
        $message = '';

        $data = $this->post();

        $update = array(
            'usuario_nombres'       => $data['usuario_nombres'],
            'usuario_apellidos'     => $data['usuario_apellidos'],
            'usuario_email'         => $data['usuario_email']
        );

        $condition = array(
            'usuario_id'    => $data['usuario_id']
        );

        $result = $this->Model->update('usuario', $update, $condition);

        if( $result ){
            $status = true;
            $message = 'La información del administrador se modificó correctamente';
        }
        else{
            $message = 'Ha ocurrido un error, intentalo nuevamente';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }

    public function obtener_texto_ingreso_get(){
        
        $data = $this->Model->obtener_texto_ingreso();

        $this->response([
            'status'    => $data ? true : false,
            'data'      => $data
        ], RestController::HTTP_OK);
    }

    public function actualizar_texto_ingreso_post(){
        $status = false;
        $message = '';

        $data = $this->post();

        if( $data ){

            $update = array(
                'texto_ingreso' => $data['texto_ingreso_nuevo']
            );

            $condition = array(
                'texto_ingreso' => $data['texto_ingreso_anterior']
            );

            $result = $this->Model->update('variables', $update, $condition);

            if( $result ){
                $status = true;
                $message = 'Se modifico correctamente el texto de ingreso';
            }
            else{
                $message = 'Ha ocurrido un error en el proceso';
            }
        }
        else{
            $message = 'Recuerde que para modificar el texto de ingreso, debe especificar el nuevo texto';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }

    public function eliminar_administrador_post(){
        
        $status = false;
        $message = '';

        $data = $this->post();

        $update = array(
            'usuario_estado'        => $data['usuario_estado']
        );

        $condition = array(
            'usuario_id'    => $data['usuario_id']
        );

        $result = $this->Model->update('usuario', $update, $condition);

        if( $result ){
            $status = true;
            $message = 'El administrador se eliminó correctamente';
        }
        else{
            $message = 'Ha ocurrido un error, intentalo nuevamente';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }

    public function borrar_datos_get(){

        $status = false;
        $message = '';

        $juegos = $this->Model->eliminar_registros_usuario_juegos();
        $test = $this->Model->eliminar_registros_usuario_test();
        $respuestas = $this->Model->eliminar_registros_usuario_respuestas();
        $usuarios = $this->Model->eliminar_registros_usuarios();

        if( $juegos && $test && $respuestas && $usuarios ){
            $status = true;
            $message = 'El borrado de los datos en la base de datos se realizó correctamente';
        }
        else{
            $message = 'El borrado de los datos en la base de datos no fue posible';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }

    private function filter_habilidades( $array, $index ){
        
        $x = 0;

        foreach ($array as $key => $item) {
            if( $item['habilidades_id'] == $index ){
                $x = $key;
            }
        }

        return $x;
    }
}