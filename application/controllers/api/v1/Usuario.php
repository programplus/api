<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Usuario extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function login_post(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $id_steam = $this->post('id_steam');

        if( $id_steam ){
            
            $data = $this->Model->obtener_usuario( NULL, $id_steam, NULL );

            if( !empty( $data ) ){
                $status = true;
                $response_http = RestController::HTTP_OK;
            }
            else{
                $message = 'El jugador no existe en la base de datos';
            }
        }
        else{
            $message = 'Debe especificar el ID de Steam del jugador para continuar con el inicio de sesión';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response_http); 
    }

    public function login_admin_post(){

        $email = $this->post('email');
		$password = $this->post('password');

        $response_http = RestController::HTTP_OK;
        $status = false;
        $message = '';

        $data = array();

        if( $email && $password ){

            $user = $this->Model->obtener_usuario(NULL, NULL, $email);

            if (!$user) {
                $message = 'El nombre de usuario no se encuentra en la base de datos';
            }

            // se envia los datos a la funcion del modelo
            if (!$this->Model->login_admin($email, $password)) {
                $user = null;
                $message = 'El usuario y/o contraseña son incorrectos';
            }
            else{
                $status = true;
                $data = [
                    'id'		=> $user->usuario_id,
                    'nombres'   => $user->usuario_nombres,
                    'apellidos' => $user->usuario_apellidos,
                    'email'		=> $user->usuario_email,
                    'rol'       => $this->Model->obtener_rol( $user->roles_roles_id )
                ];
            }
        }
        else{
            $message = 'Debe especificar el email y contraseña para iniciar sesión';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response_http); 
    }

    public function actualizar_admin_post(){

        $email_ant = $this->post('email_ant');
        $email_nue = $this->post('email_nue');
        $password = $this->post('password');

        $response_http = RestController::HTTP_NOT_FOUND;
        $status = false;
        $message = '';
        $data = array();

        $update_admin = array(
            'usuario_email'     => $email_nue,
            'usuario_password'  => password_hash($password, PASSWORD_BCRYPT)
        );

        $condition_update_admin = array(
            'usuario_email' => $email_ant
        );

        $update = $this->Model->update('usuario', $update_admin, $condition_update_admin);
        
        if( $update ){
            $status = true;
            $response_http = RestController::HTTP_OK;
            $message = 'Se modificó el admin';
            $data = $this->Model->obtener_usuario(NULL, NULL, $email_nue);
        }
        else{
            $message = 'ERROR';
        }

        $this->response([
            'status'    => true,
            'message'   => $message,
            'data'      => $data
        ], $response_http); 

    }

    public function registro_post(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';
        $data = null;

        $usuario = $this->post('usuario');

        if( $usuario ){
            
            $usuario_id = $this->Model->registrar_jugador( $usuario );
            $data = $this->Model->obtener_usuario( $usuario_id, NULL, NULL );

            if( !empty( $data ) ){

                $tests = $this->Model->obtener_tests();

                foreach ($tests as $key => $item) {
                    $array_test = [
                        'usuario_usuario_id'    => $usuario_id,
                        'test_test_id'          => $item->test_id,
                        'usuario_test_estado'   => 0
                    ];
                    $this->Model->llenar_usuario_test($array_test);
                }

                $juegos = $this->Model->obtener_juegos_nivel();

                foreach ($juegos as $key => $item) {
                    $array_juego = [
                        'usuario_usuario_id'                => $usuario_id,
                        'juegos_nivel_juegos_juegos_id'     => $item->juegos_juegos_id,
                        'juegos_nivel_nivel_nivel_id'       => $item->nivel_nivel_id,
                        'usuario_juegos_nivel_estado'       => 0
                    ];
                    $this->Model->llenar_usuario_juegos_nivel($array_juego);
                }

                $status = true;
                $response_http = RestController::HTTP_OK;
            }
            else{
                $message = 'Ha ocurrido un error en el proceso del registro del jugador';
            }
        }
        else{
            $message = 'Debe enviar información para llevar acabo el registro del usuario';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response_http);
    }

    public function obtener_lista_avatar_get( $id_steam ){
        
        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';
        $data = null;

        if( $id_steam ){
            
            $data = $this->Model->obtener_usuario( NULL, $id_steam, NULL );

            if( !empty( $data ) ){
                $status = true;
                $response_http = RestController::HTTP_OK;
            }
            else{
                $message = 'No se ha encontrado información';
            }
        }
        else{
            $message = 'Para obtener la lista de avatar debe especificar el id de steam';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => json_decode( $data->usuario_items_avatar )
        ], $response_http);
    }

    public function actualizar_lista_avatar_put(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $id_steam = $this->put('id_steam');
        $lista = $this->put('lista');

        if( ( !empty( $lista ) && gettype( $lista) === 'array' ) && ( $id_steam ) ){

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);

            if( empty($usuario) ){
                $message = 'No se ha encontrado el usuario referente al id de steam enviado';
            }
            else{

                $update_lista = array(
                    'usuario_items_avatar' => json_encode( $lista )
                );

                $condition = array(
                    'usuario_id'    => $usuario->usuario_id
                );

                $updated = $this->Model->update('usuario', $update_lista, $condition);
                
                if( $updated ){
                    $status = true;
                    $response_http = RestController::HTTP_OK;
                    $message = 'La lista de avatar se actualizó de forma correcta';
                }
                else{
                    $message = 'Ha ocurrido un error al actualiza la lista de avatar';
                }
            }
        }
        else{
            $message = 'Ha ocurrido un error, no se encontró la lista y el id de steam';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => ''
        ], $response_http);
    }

    public function eliminar_lista_avatar_get( $id_steam ){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        if( $id_steam ){

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);

            if( empty($usuario) ){
                $message = 'No se ha encontrado el usuario referente al id de steam enviado';
            }
            else{

                $update_lista = array(
                    'usuario_items_avatar' => '{}'
                );

                $condition = array(
                    'usuario_id'    => $usuario->usuario_id
                );

                $updated = $this->Model->update('usuario', $update_lista, $condition);
                
                if( $updated ){
                    $status = true;
                    $response_http = RestController::HTTP_OK;
                    $message = 'La lista de avatar se eliminar de forma correcta';
                }
                else{
                    $message = 'Ha ocurrido un error al eliminar la lista de avatar';
                }
            }
        }
        else{
            $message = 'Ha ocurrido un error, no se encontró la lista y el id de steam';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => ''
        ], $response_http);
    }

    public function obtener_lista_closet_get( $id_steam ){
        
        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';
        $data = [];

        $new_array = array();

        if( $id_steam ){
            
            $data = $this->Model->obtener_usuario( NULL, $id_steam, NULL );

            if( !empty( $data ) ){

                $old_array = json_decode( $data->usuario_items_closet );

                foreach ($old_array as $key => $item){
                    
                    $index = $this->filter_id_material( $new_array, $item->id );

                    if( !is_null( $index ) ){

                        array_push( $new_array[$index]['material'], $item->material );
                    }
                    else{
                        $new_array[] = [
                            'id'    => $item->id,
                            'material'  => [ $item->material ]
                        ];
                    }
                    /* if (!array_key_exists($item->id, $new_array)){
                        $new_array[] = $item;
                    } */
                }

                $status = true;
                $response_http = RestController::HTTP_OK;
            }
            else{
                $message = 'No se ha encontrado información';
            }
        }
        else{
            $message = 'Para obtener la lista de closet debe especificar el id de steam';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $new_array
        ], $response_http);
    }

    public function actualizar_lista_closet_put(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $usuario_items_closet = [];

        $id_steam = $this->put('id_steam');
        $lista = $this->put('lista');

        if( ( !empty( $lista ) && gettype( $lista) === 'array' ) && ( $id_steam ) ){

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);

            if( empty($usuario) ){
                $message = 'No se ha encontrado el usuario referente al id de steam enviado';
            }
            else{

                $usuario_items_closet = (array)json_decode( $usuario->usuario_items_closet );

                foreach ((array)$lista as $key => $value) {
                    array_push( $usuario_items_closet, $value );
                }

                $update_lista = array(
                    'usuario_items_closet' => json_encode( $usuario_items_closet )
                );

                $condition = array(
                    'usuario_id'    => $usuario->usuario_id
                );

                $updated = $this->Model->update('usuario', $update_lista, $condition);
                
                if( $updated ){
                    $status = true;
                    $response_http = RestController::HTTP_OK;
                    $message = 'La lista de closet se actualizó de forma correcta';
                }
                else{
                    $message = 'Ha ocurrido un error al actualiza la lista de avatar';
                }
            }
        }
        else{
            $message = 'Ha ocurrido un error, no se encontró la lista y el id de steam';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => ''
        ], $response_http);
    }

    public function eliminar_lista_closet_get( $id_steam ){
        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $usuario_items_closet = [];

        
        if( $id_steam ){

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);

            if( empty($usuario) ){
                $message = 'No se ha encontrado el usuario referente al id de steam enviado';
            }
            else{

                $update_lista = array(
                    'usuario_items_closet' => '{}'
                );

                $condition = array(
                    'usuario_id'    => $usuario->usuario_id
                );

                $updated = $this->Model->update('usuario', $update_lista, $condition);
                
                if( $updated ){
                    $status = true;
                    $response_http = RestController::HTTP_OK;
                    $message = 'La lista de closet se eliminó de forma correcta';
                }
                else{
                    $message = 'Ha ocurrido un error al eliminar la lista de avatar';
                }
            }
        }
        else{
            $message = 'Ha ocurrido un error, no se encontró la lista y el id de steam';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => ''
        ], $response_http);
    }

    public function actualizar_cargue_test_juegos_usuario_post(){
        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $email = $this->post('email');

        $usuario = $this->Model->obtener_usuario(NULL, NULL, $email);

        if( $usuario ){

            $this->Model->eliminar_registros_usuario_juegos();
            $this->Model->eliminar_registros_usuario_test();

            $tests = $this->Model->obtener_tests();

                foreach ($tests as $key => $item) {
                    $array_test = [
                        'usuario_usuario_id'    => $usuario->usuario_id,
                        'test_test_id'          => $item->test_id,
                        'usuario_test_estado'   => 0
                    ];
                    $this->Model->llenar_usuario_test($array_test);
                }

            $juegos = $this->Model->obtener_juegos_nivel();

            foreach ($juegos as $key => $item) {
                $array_juego = [
                    'usuario_usuario_id'                => $usuario->usuario_id,
                    'juegos_nivel_juegos_juegos_id'     => $item->juegos_juegos_id,
                    'juegos_nivel_nivel_nivel_id'       => $item->nivel_nivel_id,
                    'usuario_juegos_nivel_estado'       => 0
                ];
                $this->Model->llenar_usuario_juegos_nivel($array_juego);
            }

            $status = true;
            $response_http = RestController::HTTP_OK;
        }
        else{
            $message = 'ERRor';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $usuario
        ], $response_http);
    }

    private function filter_id_material($array, $elemento){

        $x = null;

        if( count( $array ) > 0 ){
            foreach ($array as $key => $value) {
                if( $value['id'] == $elemento ){
                    $x = $key;
                }
            }
        }

        return $x;
    }
}