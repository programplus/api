<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Perfiles extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function obtener_get(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $data = $this->Model->obtener_tipos_perfil();

        if( !empty( $data ) ){
            $status = true;
            $response_http = RestController::HTTP_OK;
        }
        else{
            $message = 'No se ha encontrado información';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response_http);
    }
}