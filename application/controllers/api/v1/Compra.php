<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Compra extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function compra_post(){

        $status = false;
        $message = '';

        $id_steam = $this->post('id_steam');
        $monedas = $this->post('monedas');
        $gemas = $this->post('gemas');
        $nueva_lista_closet = $this->post('closet');
        $usuario = '';

        if( $id_steam && $nueva_lista_closet ){

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);
            $monedas_restantes = (int)$usuario->usuario_monedas - (int)$monedas;
            $gemas_restantes = (int)$usuario->usuario_gemas - (int)$gemas;

            $usuario_items_closet = (array)json_decode( $usuario->usuario_items_closet );

            foreach ((array)$nueva_lista_closet as $key => $value) {
                
                $result = $this->existencia_objecto_closet( $usuario_items_closet, (array)$value );
                
                if( !$result ){
                    array_push( $usuario_items_closet, $value );
                }
            }

            $update_usuario = array(
                'usuario_items_closet'  => json_encode( $usuario_items_closet ),
                'usuario_gemas'         => $gemas_restantes,
                'usuario_monedas'       => $monedas_restantes
            );

            $condition_update_usuario = array(
                'usuario_id'    => $usuario->usuario_id
            );

            $this->Model->update('usuario', $update_usuario, $condition_update_usuario);

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);

            $status = true;
            $message = 'Se añadió correctamente los elementos a la lista del closet, de igual manera se generó el cambio en las monedas';
        }
        else{
            $message = 'Debe especificar la información necesaria para generar el proceso de compra';
        }

        $data = [
            'monedas'       => $usuario->usuario_monedas,
            'gemas'         => $usuario->usuario_gemas,
            'lista_closet'  => json_decode( $usuario->usuario_items_closet )
        ];

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], RestController::HTTP_OK);

    }

    private function existencia_objecto_closet( $lista_closet, $objeto ){

        $x = false;
        
        $array = json_decode(json_encode($lista_closet), true);

        foreach ($array as $key => $value) {
            if( ($value['id'] == $objeto['id']) && ($value['material'] == $objeto['material']) ){
                $x = true;
            }
        }

        return $x;
    }
}