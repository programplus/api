<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Lista extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function obtener_post(){

        $status = false;
        $message = '';

        $id_steam = $this->post('id_steam');
        $tests = array();
        $juegos = array();

        $nivel_usuario = null;

        $data = array();
        $cont = 0;

        if( $id_steam ){
            
            $usuario = $this->Model->obtener_usuario( NULL, $id_steam, NULL );

            if( !empty( $usuario ) ){

                $nivel_usuario = $usuario->usuario_nivel;

                $aux_juegos = $this->Model->obtener_lista_progreso_juegos( $usuario->usuario_id );
                $aux_tests = $this->Model->obtener_lista_progreso_test( $usuario->usuario_id );

                foreach ($aux_tests as $key => $test) {
                    
                    $nested_data_test['id_elemento']     = $test->test_id;
                    $nested_data_test['nombre_elemento'] = $test->test_item;
                    $nested_data_test['tipo_elemento']   = 'Cuestionario';
                    $nested_data_test['realizado']       = $test->usuario_test_estado;

                    $tests[] = $nested_data_test;
                }

                $data[] = $tests[0];

                foreach ($aux_juegos as $key_nivel => $item_nivel) {

                    foreach ($item_nivel->juegos as $key_juego => $juego) {
                        
                        $nested_data_juegos['id_elemento']     = $juego->juegos_id;
                        $nested_data_juegos['nombre_elemento'] = $juego->juegos_nombre;
                        $nested_data_juegos['tipo_elemento']   = 'Juego';
                        $nested_data_juegos['nivel']           = $item_nivel->nivel_numero;
                        $nested_data_juegos['realizado']       = $juego->usuario_juegos_nivel_estado;

                        $data[] = $nested_data_juegos;
                    }

                    $data[] = $tests[$key_nivel + 1];
                }


                $status = true;
            }
            else{
                $message = 'El jugador no existe en la base de datos';
            }
        }
        else{
            $message = 'Debe especificar el ID de Steam del jugador para continuar con el inicio de sesión';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data,
            'usuario_nivel' => $nivel_usuario
        ], RestController::HTTP_OK); 
    }

    public function actualizar_lista_progreso_post(){

        $status = false;
        $message = '';

        $id_steam = $this->post('id_steam');
        $monedas = $this->post('monedas');
        $juego_id = $this->post('juego_id');
        $nivel_id = $this->post('nivel_id');
        $gemas = $this->post('gemas');

        if( $id_steam && $juego_id && $nivel_id ){

            $usuario = $this->Model->obtener_usuario(NULL, $id_steam, NULL);
            $monedas_restantes = (int)$usuario->usuario_monedas + (int)$monedas;
            $gemas_restantes = (int)$usuario->usuario_gemas + (int)$gemas;

            $update_usuario = array(
                'usuario_nivel'     => $nivel_id,
                'usuario_gemas'     => $gemas_restantes,
                'usuario_monedas'   => $monedas_restantes
            );

            $condition_update_usuario = array(
                'usuario_id'    => $usuario->usuario_id
            );

            $this->Model->update('usuario', $update_usuario, $condition_update_usuario);

            $update_usuario_juegos_nivel = array(
                'usuario_juegos_nivel_estado'   => 1
            );

            $condition_update_usuario_juegos_nivel = array(
                'usuario_usuario_id'                    => $usuario->usuario_id,
                'juegos_nivel_juegos_juegos_id'         => $juego_id,
                'juegos_nivel_nivel_nivel_id'           => $nivel_id
            );

            $this->Model->update('usuario_juegos_nivel', $update_usuario_juegos_nivel, $condition_update_usuario_juegos_nivel);

            $status = true;
            $message = 'La lista de progreso se actualizó con exito';
        }
        else{
            $message = 'Debe especificar la información necesaria para actualizar la lista de progreso';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message
        ], RestController::HTTP_OK);
    }
}