<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Regionales extends RestController{

    public function __construct(){

        parent::__construct();
        get_cors_api();
    }

    public function obtener_regional_get(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $data = $this->Model->obtener_regional();

        if( !empty( $data ) ){
            $status = true;
            $response_http = RestController::HTTP_OK;
        }
        else{
            $message = 'No se ha encontrado información';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response_http);
    }

    public function obtener_centro_post(){

        $status = false;
        $response_http = RestController::HTTP_NOT_FOUND;
        $message = '';

        $regional_id = $this->post('regional_id') ? $this->post('regional_id') : NULL;
        $centro_nombre = $this->post('centro_nombre') ? $this->post('centro_nombre') : NULL;

        if( $regional_id ){
            
            if( $centro_nombre ){
                
                $data = $this->Model->obtener_centro( $regional_id, $centro_nombre );

                if( !empty( $data ) ){
                    $status = true;
                    $response_http = RestController::HTTP_OK;
                }
                else{
                    $message = 'No se ha encontrado información';
                }

                $this->response([
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                ], $response_http);
            }
            else{

                $data = $this->Model->obtener_centro( $regional_id );

                if( !empty( $data ) ){
                    $status = true;
                    $response_http = RestController::HTTP_OK;
                }
                else{
                    $message = 'No se ha encontrado información';
                }

                $this->response([
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                ], $response_http);
            }
        }
        else{
            $message = 'Debe especificar el identificador de la regional';
        }

        $this->response([
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ], $response_http);
    }
}