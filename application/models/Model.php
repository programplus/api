<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model {

    private $db;

	public function __construct() {

        $this->db = $this->load->database('default', TRUE);
	}

    public function get($table,$condition){
        
        foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
        }

        return $this->db->get($table)->result_object(); 
    }

    public function insert($table, $data){
        return $this->db->insert($table, $data);
    }

    public function update($table, $raw, $condition){
        $raw = (array)$raw;
        $this->db->select("*");
        $this->db->from($table);
        $this->db->limit("1");
        $results = $this->db->get()->row_array();

        $data = new StdClass;

        foreach($results as $key => $value){
            if(isset($raw[$key])){
                $data->$key = $raw[$key];
            }
        }

        foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
        }

        return $this->db->update($table, $data);
    }

    public function obtener_roles(){

        $query = $this->db
                ->select('*')
                ->from('roles r')
                ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_tipos_perfil(){

        $query = $this->db
                ->select('*')
                ->from('tipo_perfil t')
                ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_regional(){

        $query = $this->db
                ->select('*')
                ->from('regional r')
                ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_centro( $regional_id = NULL, $centro_nombre = NULL ){
        
        $query = $this->db
                ->select('*')
                ->from('centro c');

        if( $regional_id ){
            $query = $this->db->where('c.regional_regional_id', $regional_id);
        }

        if( $centro_nombre ){
            $query = $this->db->like('c.centro_nombre', $centro_nombre);
        }

        $query = $this->db->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_usuarios(){
        $query = $this->db
                ->select('
                    u.usuario_id id,
                    u.usuario_nombres nombres,
                    u.usuario_apellidos apellidos,
                    u.usuario_email email,
                    u.usuario_edad edad,
                    u.usuario_documento documento,
                    u.usuario_genero genero,
                    r.regional_nombre regional,
                    c.centro_nombre centro,
                    u.usuario_telefono telefono,
                    u.usuario_programa programa,
                    u.usuario_emp_telefono empresa_telefono,
                    u.usuario_nit empresa_nit,
                    u.usuario_empresa empresa,
                    u.usuario_ficha ficha
                ')
                ->from('usuario u')
                ->join('centro c', 'u.centro_centro_id = c.centro_id')
                ->join('regional r', 'r.regional_id = c.regional_regional_id')
                ->where('u.roles_roles_id = 2')
                ->get();

        $result = $query->result_array();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
        
    }
    public function obtener_usuario( $id_usuario = NULL, $id_steam = NULL, $email = NULL ){

        $query = $this->db
                ->select('*')
                ->from('usuario u')
                ->join('centro c', 'c.centro_id = u.centro_centro_id')
                ->join('regional r', 'r.regional_id = c.regional_regional_id');

        if( $id_usuario ){
            $query = $query->where('u.usuario_id', $id_usuario);
        }
        
        if( $id_steam ){
            $query = $query->where('u.usuario_steam', $id_steam);
        }

        if( $email ){
            $query = $query->where('u.usuario_email', $email);
        }

        $query = $query->get();
        
        $result = $query->row_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function login_admin($email, $password) {
		$this->db->select('usuario_password');
		$this->db->from('usuario');
		$this->db->where('usuario_email', $email);
        $hash = $this->db->get()->row('usuario_password');

		return $this->verify_password_hash($password, $hash);
	}

    public function registrar_jugador( $data ){

        $datos = array(
            "usuario_nombres"                                   => $data['usuario_nombres'],
            "usuario_apellidos"                                 => $data['usuario_apellidos'],
            "usuario_edad"                                      => $data['usuario_edad'],
            "usuario_tipo_doc"                                  => $data['usuario_tipo_doc'],
            "usuario_documento"                                 => $data['usuario_documento'],
            "usuario_genero"                                    => $data['usuario_genero'],
            "usuario_email"                                     => $data['usuario_email'],
            "usuario_password"                                  => isset( $data['usuario_password'] ) ? password_hash($data['usuario_password'], PASSWORD_BCRYPT) : NULL,
            "usuario_telefono"                                  => $data['usuario_telefono'],
            "usuario_steam"                                     => $data['usuario_steam'],
            "usuario_nivel"                                     => 1,
            "usuario_monedas"                                   => 1500,
            "usuario_gemas"                                     => 0,
            "usuario_items_avatar"                              => '{}',
            "usuario_items_closet"                              => '{}',
            "usuario_programa"                                  => isset( $data['usuario_programa'] ) ? $data['usuario_programa'] : NULL,
            "usuario_ficha"                                     => isset( $data['usuario_ficha'] ) ? $data['usuario_ficha'] : NULL,
            "usuario_empresa"                                   => isset( $data['usuario_empresa'] ) ? $data['usuario_empresa'] : NULL,
            "usuario_nit"                                       => isset( $data['usuario_nit'] ) ? $data['usuario_nit'] : NULL,
            "usuario_emp_telefono"                              => isset( $data['usuario_emp_telefono'] ) ? $data['usuario_emp_telefono'] : NULL,
            "usuario_estado"                                    => 1,
            "roles_roles_id"                                    => 2,
            "tipo_perfil_tipo_perfil_id"                        => $data['tipo_perfil_tipo_perfil_id'],
            "centro_centro_id"                                  => $data['centro_centro_id']
        );

        $this->db->insert('usuario', $datos);
        return $this->db->insert_id();
    }

    public function obtener_rol($rol_id){
        $this->db->select('*');
		$this->db->from('roles');
		$this->db->where('roles_id', $rol_id);
		return $this->db->get()->row();
    }

    public function obtener_juegos_nivel(){

        $query = $this->db
                ->select('*')
                ->from('juegos_nivel jn')
                ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_test( $test_id ){

        $query = $this->db
                ->select('*')
                ->from('test t')
                ->where('t.test_id', $test_id)
                ->where('t.test_estado = 1')
                ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_tests(){

        $query = $this->db
                ->select('*')
                ->from('test t')
                ->where('t.test_estado = 1')
                ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_tests_usuarios( $usuario_id ){
        
        $query = $this->db
                ->select('t.test_id, t.test_item')
                ->from('test t')
                ->join('usuario_test ut', 't.test_id = ut.test_test_id', 'inner')
                ->where('t.test_estado = 1')
                ->where('ut.usuario_usuario_id', $usuario_id)
                ->get();

        $result = $query->result_object();
        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_estado_test( $usuario_id, $test_id ){
        $query = $this->db
                    ->select('usuario_test_estado')
                    ->from('usuario_test')
                    ->where('usuario_usuario_id', $usuario_id)
                    ->where('test_test_id', $test_id)
                    ->get();
        $result = $query->row('usuario_test_estado');

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_preguntas_test( $test_id ){

        $query = $this->db
                ->select('p.preguntas_id pregunta_id, p.preguntas_item pregunta, p.preguntas_orden pregunta_orden, p.habilidades_habilidades_id habilidad, p.test_test_id test_id')
                ->from('preguntas p')
                ->where('p.preguntas_estado = 1')
                ->where('p.test_test_id', $test_id)
                ->get();

        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_respuestas_test( $preguntas_id ){

        $query = $this->db
                ->select('r.respuestas_id respuesta_id, r.respuestas_item respuesta, r.preguntas_preguntas_id pregunta_id')
                ->from('respuestas r')
                ->where('r.respuestas_estado = 1')
                ->where('r.preguntas_preguntas_id', $preguntas_id)
                ->get();

        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function llenar_usuario_test( $datos ){
        $this->db->insert('usuario_test', $datos);
        return $this->db->affected_rows() > 0 ? true : false;
    }

    public function llenar_usuario_juegos_nivel( $datos ){
        $this->db->insert('usuario_juegos_nivel', $datos);
        return $this->db->affected_rows() > 0 ? true : false;
    }

    public function eliminar_registros_usuario_juegos(){

        return $this->db->empty_table('usuario_juegos_nivel');
    }

    public function eliminar_registros_usuario_test(){

        return $this->db->empty_table('usuario_test');
    }

    public function eliminar_registros_usuario_respuestas(){

        return $this->db->empty_table('usuario_respuestas');
    }

    public function eliminar_registros_usuarios(){
        $this->db->where('roles_roles_id = 2');
        return $this->db->delete('usuario');
    }

    public function obtener_lista_progreso_juegos( $usuario_id ){
        
        $query = $this->db->select('n.nivel_id, n.nivel_numero')
                            ->from('nivel n')
                            ->get();

        $result = $query->result_object();

        $i = 0;
        foreach ($result as $key => $item) {
            $result[$i]->juegos = $this->obtener_nivel_juego( $usuario_id, $item->nivel_id );
            $i++;
        }

        return $result;
    }

    public function obtener_nivel_juego( $usuario_id, $nivel_id ){
        $query = $this->db->select('j.juegos_id, j.juegos_nombre, ujn.usuario_juegos_nivel_estado')
                            ->from('juegos j')
                            ->join('usuario_juegos_nivel ujn', 'j.juegos_id = ujn.juegos_nivel_juegos_juegos_id')
                            ->where('ujn.juegos_nivel_nivel_nivel_id', $nivel_id);

        if( $usuario_id ){
            $query = $query->where('ujn.usuario_usuario_id', $usuario_id);
        }

        return $query->get()->result_object();
    }

    public function obtener_lista_progreso_test( $usuario_id ){
        
        $query = $this->db->select('t.test_id, t.test_item, ut.usuario_test_estado')
                            ->from('test t')
                            ->join('usuario_test ut', 't.test_id = ut.test_test_id')
                            ->join('usuario u', 'u.usuario_id = ut.usuario_usuario_id');
        
        if( $usuario_id ){
            $query = $query->where('ut.usuario_usuario_id', $usuario_id);
        }   
                    
        $query = $query->get();

        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_habilidades(){

        $query = $this->db
                    ->select('h.habilidades_id habilidad_id, h.habilidades_item habilidad_nombre')
                    ->from('habilidades h')
                    ->get();
        
        $result = $query->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function cantidad_habilidades_preguntas_test( $test_id, $habilidad_id ){


        /* $this->db->where('test_test_id',$test_id);
            $this->db->where('tipo_pregunta_tipo_pregunta_id = 2');
            $this->db->where('habilidades_habilidades_id',$habilidad_id);
            return $this->db->count_all_results('preguntas');
        */
            /* $query = $this->db
                        ->select('*')
                        ->from('preguntas p')
                        ->where('test_test_id', $test_id)
                        ->where('habilidades_habilidades_id', $habilidad_id)
                        ->where('tipo_pregunta_tipo_pregunta_id = 2')
                        ->get();

            return $query->num_rows(); */

            $sql = "SELECT * FROM preguntas p WHERE p.test_test_id = " . $test_id ." AND p.habilidades_habilidades_id = " . $habilidad_id ." AND p.tipo_pregunta_tipo_pregunta_id = 1";

            return $this->db->query( $sql )->num_rows();

    }

    public function obtener_respuestas_usuario( $usuario_id = NULL, $test_id = NULL ){

        $sql = "SELECT p.preguntas_id, p.habilidades_habilidades_id, p.test_test_id, r.respuestas_correct
        FROM preguntas p 
        INNER JOIN usuario_respuestas ur ON p.preguntas_id = ur.preguntas_preguntas_id
        INNER JOIN respuestas r ON r.respuestas_id = ur.respuestas_respuestas_id
        WHERE ur.usuario_usuario_id = " . $usuario_id;

        if( $test_id ){
            $sql .= " AND p.test_test_id = " . $test_id;
        }

        $result = $this->db->query($sql)->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_usuario_respuestas_test( $usuario_id = NULL, $test_id = NULL, $valid = NULL ){
        $sql = "SELECT p.preguntas_id, p.habilidades_habilidades_id, p.test_test_id, p.preguntas_item, r.respuestas_id, r.respuestas_item, r.respuestas_correct, p.tipo_pregunta_tipo_pregunta_id tipo_pregunta
        FROM preguntas p 
        INNER JOIN usuario_respuestas ur ON p.preguntas_id = ur.preguntas_preguntas_id
        INNER JOIN respuestas r ON r.respuestas_id = ur.respuestas_respuestas_id";

        if( $usuario_id ){

            $sql .= " WHERE ur.usuario_usuario_id = " . $usuario_id;
        }

        if( $test_id ){
            $sql .= " AND p.test_test_id = " . $test_id;
        }
        
        if( $valid ){
            $sql .= " AND ur.usuario_respuestas_id IN
            (
                SELECT MAX(usuario_respuestas_id) FROM usuario_respuestas 
                GROUP BY usuario_respuestas_id
            )";
        }

        $result = $this->db->query($sql)->result_object();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function contar_pregs_habil_test( $test_id ){
        $sql = "SELECT h.habilidades_id, h.habilidades_item, (
            SELECT COUNT(*) FROM preguntas p WHERE p.habilidades_habilidades_id = h.habilidades_id AND p.tipo_pregunta_tipo_pregunta_id = 1 AND p.test_test_id = (
                SELECT t.test_id FROM test t WHERE t.test_id = " . $test_id . "
            )
        ) cant_preg FROM habilidades h;";

        $result = $this->db->query($sql)->result_array();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    public function obtener_texto_ingreso(){
        $query = $this->db->select('v.texto_ingreso')
                        ->from('variables v')
                        ->get();

        $result = $query->row();

        if( $result ){
            return $result;
        }
        else{
            return NULL;
        }
    }

    private function verify_password_hash($password, $hash) {
		return password_verify($password, $hash);
			
	}
}